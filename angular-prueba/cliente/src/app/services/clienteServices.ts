import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ClienteModel } from "../models/ClienteModel";

@Injectable({
    providedIn: 'root'
})

export class ClienteServices {

    private url:string='http://localhost:8080'

    constructor(private http:HttpClient){}

    
    obtenerHistorico(cliente:string):Observable<any>{
        return this.http.post(`${this.url}/srv-entrego-web/rest/cliente/obtieneDatosCliente`,{cuenta:cliente});
    }

    obtenerTotales(cliente:string):Observable<any>{
        return this.http.post(`${this.url}/srv-entrego-web/rest/cliente/obtieneTotales`,{cuenta:cliente});
    }
}