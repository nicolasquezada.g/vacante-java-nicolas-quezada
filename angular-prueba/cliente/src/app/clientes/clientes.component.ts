import { Component, OnInit } from '@angular/core';
import { ClienteServices } from '../services/clienteServices';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  providers:[ClienteServices]
})
export class ClientesComponent implements OnInit {

  cuenta:string = '';

  listaHistorico=[]
  listaTotales = []
  control = 0

  constructor(private clienteServices:ClienteServices) { }

  ngOnInit(): void {
  }

  obtenerHistorico(){
    this.listaHistorico = [];
    console.log(this.cuenta);
    this.clienteServices.obtenerHistorico(this.cuenta).subscribe(res=>{
      this.listaHistorico = res;
      this.obtieneTotales();
      this.control = 1
    })
  }

  obtieneTotales(){
    this.clienteServices.obtenerTotales(this.cuenta).subscribe(res=>{
      this.listaTotales = res;
    })
  }

}
