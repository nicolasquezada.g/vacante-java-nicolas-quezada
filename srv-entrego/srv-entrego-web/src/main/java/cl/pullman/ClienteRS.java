package cl.pullman;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import cl.pullman.ejb.patron.to.ClienteTO;
import cl.pullman.ejb.patron.to.TotalesTO;
import cl.pullman.ejb.sessions.stateless.facade.interfaces.ClienteFacadeLocal;

@Path("/cliente")
public class ClienteRS {


	@Inject ClienteFacadeLocal clienteFacadeLocal;
	private Logger log = Logger.getLogger(ClienteRS.class);

	@POST
	@Path("/obtieneDatosCliente")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public List<ClienteTO> obtieneDatosCliente(ClienteTO busca) {
		log.info("[inicia metodo] obtieneDatosCliente");
		return clienteFacadeLocal.obtieneDatosCliente(busca);	
	}
	
	@POST
	@Path("/obtieneTotales")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public List<TotalesTO> obtenerTotales(ClienteTO busca) {
		log.info("[inicia metodo] obtenerTotales");
		return clienteFacadeLocal.obtenerTotales(busca);	
	}
	
}
