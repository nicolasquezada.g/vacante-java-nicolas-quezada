package cl.pullman.ejb.sessions.stateless.facade.interfaces;

import java.util.List;

import cl.pullman.ejb.entity.Cliente;
import cl.pullman.ejb.patron.to.ClienteTO;
import cl.pullman.ejb.patron.to.TotalesTO;

public interface ClienteFacadeLocal {

	public List<ClienteTO> obtieneDatosCliente(ClienteTO busca);
	
	public List<TotalesTO> obtenerTotales(ClienteTO busca);
	
}
