package cl.pullman.ejb.patron.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.logging.Logger;

import cl.pullman.ejb.entity.Cliente;
import cl.pullman.ejb.patron.to.ClienteTO;
import cl.pullman.ejb.patron.to.TotalesTO;

public class ClienteDAO extends BaseDAO<Cliente> {

	public ClienteDAO(Class<Cliente> entityClass, EntityManager em) {
		super(entityClass, em);
		// TODO Auto-generated constructor stub
	}
	

	private Logger log = Logger.getLogger(ClienteDAO.class);
	
	public List<ClienteTO> obtieneDatosCliente(ClienteTO busca){
		List<ClienteTO> cliList = new ArrayList<ClienteTO>();
		Query q = this.getEm().createNativeQuery("select c.nombre, descripcion, c.precio , c.sucursal  from cliente c where c.cuenta = :cuenta \r\n"
				+ "group by c.descripcion, c.precio")
				.setParameter("cuenta", busca.getCuenta().trim());
		try {
			List<Object[]> obj = q.getResultList();
			if (obj.size()!=0) {
				for (Object[] data : obj) {
					ClienteTO cli = new ClienteTO();
					cli.setNombre(data[0].toString());
					cli.setDescripcion(data[1].toString());
					cli.setPrecio(data[2].toString());
					cli.setSucursal(data[3].toString());
					cliList.add(cli);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Error al obtener los datos de los clientes");
		}
		return cliList;
	}
	
	public List<TotalesTO> obtenerTotales(ClienteTO busca){
		List<TotalesTO> totaList = new ArrayList<TotalesTO>();
		Query q = this.getEm().createNativeQuery("select sum(c2.precio) as Recarga, (select sum(c3.precio)  from cliente c3 where descripcion != 'recarga') as Compras, \r\n"
				+ "(select sum(c2.precio)  from cliente c2 where descripcion = 'recarga') - (select sum(c3.precio) as recarga  from cliente c3 where descripcion != 'recarga') as total \r\n"
				+ "from cliente c2 where descripcion = 'recarga' and c2.cuenta = :cuenta")
				.setParameter("cuenta", busca.getCuenta().trim());
		try {
			List<Object[]> obj = q.getResultList();
			if (obj.size()!=0) {
				for(Object[] data:obj) {
					TotalesTO total = new TotalesTO();
					total.setRecargas(data[0].toString());
					total.setCompras(data[1].toString());
					total.setTotal(data[2].toString());
					totaList.add(total);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return totaList;
	}

}
