package cl.pullman.ejb.sessions.stateless.facade;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cl.pullman.ejb.entity.Cliente;
import cl.pullman.ejb.patron.dao.ClienteDAO;
import cl.pullman.ejb.patron.to.ClienteTO;
import cl.pullman.ejb.patron.to.TotalesTO;
import cl.pullman.ejb.sessions.stateless.facade.interfaces.ClienteFacadeLocal;

@Stateless
@Local(ClienteFacadeLocal.class)
public class ClienteFacade implements ClienteFacadeLocal {

	@PersistenceContext(unitName = "MySqlDS")
	private EntityManager em;

	@Override
	public List<ClienteTO> obtieneDatosCliente(ClienteTO busca) {
		ClienteDAO dao = new ClienteDAO(Cliente.class, em);
		return dao.obtieneDatosCliente(busca);
	}

	@Override
	public List<TotalesTO> obtenerTotales(ClienteTO busca) {
		ClienteDAO dao = new ClienteDAO(Cliente.class, em);
		return dao.obtenerTotales(busca);
	}
	
}
