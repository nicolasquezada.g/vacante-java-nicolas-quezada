package cl.pullman.ejb.patron.to;

public class TotalesTO {
	
	private String recargas;
	private String compras;
	private String total;
	
	public String getRecargas() {
		return recargas;
	}
	public void setRecargas(String recargas) {
		this.recargas = recargas;
	}
	public String getCompras() {
		return compras;
	}
	public void setCompras(String compras) {
		this.compras = compras;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	
}
