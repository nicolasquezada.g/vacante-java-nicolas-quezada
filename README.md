PARA INICIAR EL PROYECTO DE ANGULAR DEBE HACER LO SIGUIENTE
1- npm install --force
2- npm start

PARA HACER FUNCIONAR EL BACKEND
1- descargar servidor wildfly 26.1.2
2- descomprimir 'driver mysql wildfly.rar' en la siguiente ruta: "wildfly-26.1.2.Final\modules\system\layers\base\com\mysql\driver\main" (si no existe debe crearla)
3- debe agregar al archivo "standalone.xml" la siguiene linea: "<driver name="mysql" module="com.mysql.driver"/>" dentro de "<drivers></drivers>"
4- debe crear un usuario de administracion de wildfly ejecutando el archivo "add-user.bat" en la ruta: 'wildfly-26.1.2.Final\bin' y seguir los pasos correpondientes
5- iniciar el servidor wildfly abriendo el archivo "standalone.bat" en la ruta: 'wildfly-26.1.2.Final\bin'
6- debe configurar la conexion a la base de datos en la consola de administracion de wildfly poniendo el sigiente enlace en el navegador: "http://127.0.0.1:9990/console/index.html"
7- Luego de entrar al sistema de administracion selecciona "Configuration/Subsystems/Datasources & Drivers/Datasources" y en la pestaña que se abrió agrega la nueva conexion, la cual se debe llamar "MySqlDS"
8- teniendo abierto el codigo en ecilpse, hacer click derecho en el proyecto general o el package que sostiene los 3 proyectos (ear, ejb y web), seleccionar "Run As", "maven build" y en 'Goals' escribir: "clean package wildfly:deploy"
9- Una vez se complete la carga, deberia estar listo para hacerle ping a los servicios










```

### 3.- Ejercicio
 MYSQL , Java ,ORM el siguientes casos de uso, siguiendo los principios SOLID y patrones de arquitectura:

* Actualmente los clientes cuentan con una tarjeta electrónica, donde realizan recargas y comprar en cualquier punto de venta.

* Se solicita crear los siguientes casos de uso.
* Permitir agregar saldos si el cliente esta activo
* Permitir comprar un producto si este cuenta con saldo.
* Permitir y consultar el histórico de comprar y recargas.
* Permitir la edición del nombre del cliente.

*  Vistas
```

1.- Cliente
Nombre : Eduardo Padilla
Cuenta: 12234444

2.- Compras
Productos: Coca
Precio : 3500

3.- Recarga
Cuenta : 12234444
Cantidad : 5000

4.- Histórico
   Eduardo Padilla  
   Cta :12234444 

```
 Vista Histórico
 
|Descripción| Compras| Recargas|
| --- | --- |--- |
|Recarga	| 		 | 5000    |
|Coca		| 3500	 |         |
|Papas		| 1500	 |         |
|Recarga	|        |  1000   |
|TOTAL	    |   5000 |  6000   |
|Saldo	    |    |  	+1000   |


